package com.hhl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetYour11Application {

	public static void main(String[] args) {
		SpringApplication.run(GetYour11Application.class, args);
	}

}
